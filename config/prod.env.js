'use strict'
module.exports = {
  NODE_ENV: '"production"',
  VUE_APP_URL_API_BACKEND: '"https://www.targetsia.xyz"',
  VUE_APP_URL_API_V2: '"https://www.targetsia.xyz"',

  /** List documents for client */
  DOCUMENTS_CLIENT_LIST: [
    "rut",
    "cc_representante_legal",
    "camara_comercio",
    "referencias_comerciales",
    "certificado_residencia"
  ],

  /** List documents for candidate */
  DOCUMENTS_CANDIDATE_LIST: [
      "documento_de_identidad",
      "certificado_cuenta_bancaria",
      "rut",
      "firma_digital",
      "recibo_servicio_publico_residencia",
      "renuncia",
      "hoja_de_vida"
  ]
}
