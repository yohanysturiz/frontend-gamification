'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  VUE_APP_URL_API_BACKEND: '"https://www.targetsia.xyz"',
  VUE_APP_URL_API_V2: '"https://www.targetsia.xyz"',
  BASE_URL: '"https://www.targetsia.xyz"',

  /** List documents for client */
  DOCUMENTS_CLIENT_LIST: "rut, cc_representante_legal, camara_comercio, certificado_residencia, referencias_comerciales",

    /** List documents for candidate */
    DOCUMENTS_CANDIDATE_LIST: [
        "documento_de_identidad",
        "certificado_cuenta_bancaria",
        "rut",
        "firma_digital",
        "recibo_servicio_publico_residencia",
        "renuncia",
        "hoja_de_vida"
    ]
})