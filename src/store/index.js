import { createStore } from 'vuex';

const store = createStore({
    state() {
        return {
            token: null,
            user: {}
        };
    },
    mutations: {
        setToken(state, token) {
            state.token = token;
        },
        setUser(state, user) {
            state.user = user
        },
        clearToken(state) {
            console.log("Delete localstorage")
            state.token = null;
            state.user = {}
        }
    },
    actions: {
        rehydrate({ commit }) {
            if (localStorage.hasOwnProperty('api_token')) {
                const token = localStorage.getItem('api_token')
                const user = localStorage.getItem('user')
                if (token && user) {
                    commit('setUser', user)
                    commit('setToken', token)
                }
            }
        }
    },
    plugins: [
        store => {
            store.subscribe((mutation, state) => {
                if (state.token !== null) {
                    localStorage.setItem('api_token', state.token)
                    localStorage.setItem('user', state.user)
                }
            })
        }
    ],
    getters: {
        isLoggedIn(state) {
            if (localStorage.hasOwnProperty('api_token')) {
                if (localStorage.getItem('api_token') !== null) {
                    return true;
                }

                return false;
            }
            return false;
        },
    },
});

store.dispatch('rehydrate')
export default store;