export const socials = [
    {
        name: "github",
        slug: "Regístrate con Github",
        icon: require("../../assets/github.png"),
    },
    {
        name: "google",
        slug: "Regístrate con Google",
        icon: require("../../assets/google-logo.png"),
    },
    {
        name: "linkedin",
        slug: "Regístrate con Linkedin",
        icon: require("../../assets/linkedin-logo.png"),
    }
];