const PAYPAL = "Paypal";
const BANCOLOMBIA = "Bancolombia";
const BANCOBOGOTA = "Banco de Bogota";
const BANCOPOPULAR = "Banco Popular";
const BANCODAVIVIENDA = "Banco Davivienda";
const BANCOITAU = "Banco ITAU corpbanca";
const BANCOCITYBANK = "Banco citibank";
const BANCOBBVA = "Banco BBVA";
const BANCOOCCIDENTE = "Banco de occidente";
const BANCOFALABELLA = "Banco Falabella";
const BANCOAVVILLAS = "Banco AV Villas";
const NEQUI = "Nequi";
const BANESCOPANAMA = "Banesco Panama";
const BANKAMERICA = "Bank Of America";
const JPMORGAN = "JPMorgan";

const filterPayments = [
    {
        text: PAYPAL,
        value: PAYPAL,
    },
];

const paymentsMetohds =  {
    PAYPAL: PAYPAL,
    BANCOLOMBIA: BANCOLOMBIA,
    BANCOBOGOTA: BANCOBOGOTA,
    BANCOPOPULAR: BANCOPOPULAR,
    BANCODAVIVIENDA: BANCODAVIVIENDA,
    BANCOITAU: BANCOITAU,
    BANCOCITYBANK: BANCOCITYBANK,
    BANCOBBVA: BANCOBBVA,
    BANCOOCCIDENTE: BANCOOCCIDENTE,
    BANCOFALABELLA: BANCOFALABELLA,
    BANCOAVVILLAS: BANCOAVVILLAS,
    NEQUI: NEQUI,
    BANESCOPANAMA: BANESCOPANAMA,
    BANKAMERICA: BANKAMERICA,
    JPMORGAN: JPMORGAN,
};

// $paymentMethods.PAYPAL

paymentsMetohds.install = function(Vue) {
    Vue.prototype.$paymentMethods = paymentsMetohds;
    Vue.prototype.$filterPaymentMethods = filterPayments;

}

export default paymentsMetohds;
