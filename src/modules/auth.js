export default {
    namespaced: true,
    state: {
        authenticate: false,
        user_session: null,
    },
    mutations: {
        changeAuth(state, user){
            state.authenticate = true;
            state.user_session = JSON.parse(localStorage.getItem('user'))
        },
        logoutUser(state, payload){
            state.authenticate = false;
            state.user_session = null;
        }
    },
    actions: {

    },
    getters: {

    }
}