export default {
    namespaced: true,
    state: {
        client: [],
    },
    mutations: {
        setClient(state, payload) {
            state.client = payload;
        },
    },
    actions: {
        client({commit}, payload) {
            commit('CLIENT', payload);
        },
    },
    getters: {
        getClient: state => {
            return state.client;
        },
    }
}
