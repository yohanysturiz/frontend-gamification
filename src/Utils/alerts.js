export const alerts = {
    methods: {
        alertMessage(msg, type_message) {
            this.$notify({
                title: 'Success!',
                message: msg,
                position: 'bottom-right',
                type: type_message
            });
        },
    }
}

