export const contractClient = {
    data: function() {
        return {
        }
    },
    methods: {
        getContractCustomer(contract_data){
            console.log("Cliente")
            return `<div style="text-align: justify;">
            <p style="text-align: center;"><b>CONTRATO DE PRESTACIÓN DE SERVICIOS PROFESIONALES, CELEBRADO ENTRE
                IMUKO SAS y ${contract_data.full_name_legal}
               </b></p>
                <p style="text-align: justify;">Entre los suscritos <strong> 
                    ${contract_data.full_name_legal}
                  </strong> identificado con <strong> ${contract_data.type_document_representative_legal} No.
                  ${contract_data.number_document_representative_legal} </strong> expedida en ${contract_data.city_expedition_name_representative_legal},
                domiciliado y residente en la ciudad de ${contract_data.city_resident_name_representative_legal},
                actuando en su calidad de Representante Legal de <strong> ${contract_data.company_name}, </strong> sociedad identificada con el <strong> NIT ${contract_data.rut}
                  </strong> domiciliada en ${contract_data.city_resident_name_representative_legal}, y quien para los efectos del presente
                contrato se denominará,<strong> EL CONTRATANTE </strong>por una parte; y por la otra, <strong>TATIANA
                    PRIETO TRIANA, </strong>identificada con cédula de ciudadanía <strong>No. 52.981.604</strong>
                expedida en Bogotá D.C. domiciliado y residente en la ciudad de Bogotá D.C, actuando en su calidad
                de Representante Legal de IMUKO S.A.S
                    <span style="font-weight: 400;">sociedad identificada con el </span><b>NIT 901250358 - 1</b><span
                    style="font-weight: 400;"> como consta en el Certificado de Existencia y Representaci&oacute;n Legal
                    expedido por la C&aacute;mara de Comercio de Bogot&aacute; D.C quien para los efectos del presente
                    contrato se denominar&aacute; </span><b>EL CONTRATISTA, </b><span style="font-weight: 400;">hemos
                    convenido en celebrar el presente </span><b>CONTRATO DE PRESTACI&Oacute;N DE SERVICIOS PROFESIONALES,
                </b><span style="font-weight: 400;">teniendo en cuenta las siguientes </span><b>consideraciones:&nbsp;</b>
            </p>
            <p><b>PRIMERA: </b><span style="font-weight: 400;">Que </span><b>EL CONTRATISTA </b><span
                    style="font-weight: 400;">en su labor de proveedor de desarrollo de software, tiene la capacidad
                    operativa suficiente para el desarrollo del objeto del presente contrato.&nbsp;</span></p>
            <p><b>SEGUNDA: </b><span style="font-weight: 400;">Que </span><b>EL CONTRATANTE </b><span
                    style="font-weight: 400;">requiere la prestaci&oacute;n de servicios de portafolio del
                </span><b>CONTRATISTA.&nbsp;</b></p>
            <p><b>TERCERA: </b><span style="font-weight: 400;">Que, para el desarrollo del objeto del presente contrato,
                </span><b>EL CONTRATANTE </b><span style="font-weight: 400;">requiere entonces de los servicios que
                    prestar&aacute; </span><b>EL CONTRATISTA&nbsp;</b></p>
            <p><b>CUARTA: AUSENCIA DE RELACI&Oacute;N LABORAL</b><span style="font-weight: 400;">. Queda entendido y
                    expresamente aceptado por las partes que, entre las mismas, no existir&aacute; relaci&oacute;n laboral
                    alguna. Tampoco existir&aacute; relaci&oacute;n laboral alguna entre </span><b>EL CONTRATANTE </b><span
                    style="font-weight: 400;">y los trabajadores, empleados, personal, asesores y subcontratistas del
                </span><b>CONTRATISTA. EL CONTRATISTA </b><span style="font-weight: 400;">tendr&aacute; plena
                    autonom&iacute;a t&eacute;cnica, administrativa, financiera y directiva, asumir&aacute; sus propios
                    riesgos, celebrar&aacute; contratos escritos con el personal que emplee para la ejecuci&oacute;n del
                    presente contrato y en la celebraci&oacute;n, ejecuci&oacute;n y terminaci&oacute;n de tales contratos
                    dar&aacute; estricto cumplimiento a sus obligaciones laborales derivadas de los mismos como verdadero y
                    &uacute;nico empleador de dichos trabajadores, de igual forma ninguna de las partes ser&aacute;
                    solidario uno con el otro de cualquier obligaci&oacute;n laboral.&nbsp;</span></p>
            <p><span style="font-weight: 400;">Por lo anterior, las partes celebran el presente contrato, el cual se
                    regir&aacute; por las siguientes cl&aacute;usulas:&nbsp;</span></p>
            <p><b>CL&Aacute;USULA PRIMERA: OBJETIVOS DEL CONTRATO&nbsp;</b></p>
            <p><span style="font-weight: 400;">El </span><b>OBJETO GENERAL </b><span style="font-weight: 400;">del contrato
                    consiste en ejecuci&oacute;n de actividades de desarrollo en <strong>${contract_data.lenguage_programming},
                    </strong>seg&uacute;n requerimientos de</span><b>l CONTRATANTE </b><span
                    style="font-weight: 400;">hacia </span><b>EL CONTRATISTA.&nbsp;</b></p>
            <p><b>CL&Aacute;USULA SEGUNDA: ACTIVIDADES ESPEC&Iacute;FICAS DEL CONTRATO&nbsp;</b></p>
            <p><span style="font-weight: 400;">Las actividades espec&iacute;ficas a desarrollar para la prestaci&oacute;n de
                    los servicios profesionales, de conformidad con los requerimientos efectuados por el solicitante
                    ser&aacute;n las siguientes:&nbsp;</span></p>
            <ul>
                <li><span style="font-weight: 400;"> Ejecuci&oacute;n de actividades de desarrollo de software en <strong>${contract_data.lenguage_programming},
                </strong> </span><b></b><span style="font-weight: 400;">de conformidad con los requerimientos e
                        instrucciones del</span><b> CONTRATANTE </b><span style="font-weight: 400;">hacia </span><b>EL
                        CONTRATISTA.&nbsp;</b></li>
            </ul>
            <p><b>CL&Aacute;USULA TERCERA: ENTREGABLES&nbsp;</b></p>
            <p><span style="font-weight: 400;">En desarrollo de las cl&aacute;usulas Primera y Segunda del presente
                    contrato, </span><b>EL CONTRATISTA </b><span style="font-weight: 400;">deber&aacute; presentar los
                    entregables en los que d&eacute; cuenta de las actuaciones realizadas.&nbsp;</span></p>
            <p><span style="font-weight: 400;">Estos deber&aacute;n responder a los requerimientos efectuados por el
                    solicitante de conformidad con las especificaciones t&eacute;cnicas correspondientes.&nbsp;</span></p>
            <p><b>CL&Aacute;USULA CUARTA: VALOR DEL CONTRATO Y FORMA DE PAGO&nbsp;</b></p>
            <p><span style="font-weight: 400;">El valor total mensual del presente contrato, corresponde a la suma de
                </span><strong>${contract_data.contract_value_letter} ($ ${contract_data.contract_value_number} COP) </strong>
                <span style="font-weight: 400;">por mes, por un periodo
                    de </span><b> ${contract_data.contract_duration}</b><span style="font-weight: 400;"> , de manera remota,&nbsp; suma de dinero
                    que se cancelar&aacute; de manera mensual, con factura de cobro.&nbsp;</span></p>
                    <p>Fecha de inicio: ${contract_data.start_contract_date} </p>
            <p></p>
            <p><b>CL&Aacute;USULA QUINTA: INICIO DEL CONTRATO&nbsp;</b></p>
            <p><span style="font-weight: 400;">El contrato se dar&aacute; por iniciado con la firma de este documento y un
                    acta de inicio previamente suscrita entre las partes.&nbsp;</span></p>
            <p><b>CL&Aacute;USULA SEXTA: DECLARACIONES DEL CONTRATISTA&nbsp;</b></p>
            <p><b>EL CONTRATISTA </b><span style="font-weight: 400;">hace las siguientes declaraciones:&nbsp;</span></p>
            <ul>
                <li><span style="font-weight: 400;"> </span><span style="font-weight: 400;">Conoce y acepta los Documentos
                        del Proceso&nbsp;</span></li>
                <li><span style="font-weight: 400;"> </span><span style="font-weight: 400;">Tuvo la oportunidad de solicitar
                        aclaraciones y modificaciones a los mismos, y recibi&oacute; del </span><b>CONTRATANTE </b><span
                        style="font-weight: 400;">respuesta oportuna a cada una de las solicitudes.&nbsp;</span></li>
                <li><span style="font-weight: 400;"> </span><span style="font-weight: 400;">Se encuentra debidamente
                        facultado
                        para suscribir el presente Contrato </span></li>
            </ul>
    
            <p><b>CL&Aacute;USULA S&Eacute;PTIMA: PLAZO DE EJECUCI&Oacute;N&nbsp;</b></p>
            <p><span style="font-weight: 400;">El plazo de ejecuci&oacute;n del presente contrato ser&aacute; de un periodo
                    de</span><b> ${contract_data.contract_duration} </b><span style="font-weight: 400;">contados a partir del d&iacute;a de la firma
                    de este documento y un acta de inicio y en que se inicien de manera formal las actividades. Los
                    Desarrolladores del </span><b>CONTRATISTA , </b><span style="font-weight: 400;">trabajar&aacute;n de
                    Lunes a Viernes, durante</span><b> 6 horas diarias,</b><span style="font-weight: 400;"> dentro del
                    horario laboral (Lunes -Viernes 8:00 am a 5 pm), durante este periodo de</span><b> ${contract_data.contract_duration} &nbsp;
                </b><span style="font-weight: 400;">a trav&eacute;s del recurso que disponga para tal efecto. Este plazo
                    inicial puede ser ampliado de com&uacute;n acuerdo entre las partes si las mismas luego de finalizado el
                    periodo inicial y por necesidades propias del objeto del contrato, requieren efectuar tal
                    ampliaci&oacute;n del periodo, la cual deber&aacute; constar en un documento anexo al presente contrato
                    firmado por ambas partes.&nbsp;</span></p>
            <p><b>El CONTRATANTE</b><span style="font-weight: 400;">, no podr&aacute; contratar directamente a ning&uacute;n
                    recurso suministrado por </span><b>EL CONTRATISTA </b><span style="font-weight: 400;">de manera directa,
                    antes de finalizado el periodo de </span><b>TRES (3) MESES </b><span
                    style="font-weight: 400;">despu&eacute;s del inicio del contrato. </span><b>EL CONTRATANTE </b><span
                    style="font-weight: 400;">podr&aacute; conservar el recurso que disponga </span><b>EL CONTRATISTA
                </b><span style="font-weight: 400;">s&oacute;lo si se presentan las siguientes condiciones:&nbsp;</span></p>
            <p><b>PAR&Aacute;GRAFO: </b><span style="font-weight: 400;">Luego de finalizado el periodo de </span><b>TRES (3)
                    MESES, EL CONTRATANTE </b><span style="font-weight: 400;">podr&aacute; conservar el recurso del que
                    disponga </span><b>EL CONTRATISTA </b><span style="font-weight: 400;">si se presentan las siguientes
                    condiciones:&nbsp;</span></p>
            <ol>
                <li><span style="font-weight: 400;">a) Si el recurso dispuesto por </span><b>EL CONTRATISTA </b><span
                        style="font-weight: 400;">desea continuar las labores con </span><b>EL CONTRATANTE, </b><span
                        style="font-weight: 400;">podr&aacute; continuar de manera directa con las actividades luego de
                        finalizado el periodo de </span><b>TRES (3) MESES. </b><span style="font-weight: 400;">En caso que
                        el recurso del que disponga </span><b>EL CONTRATISTA </b><span
                        style="font-weight: 400;">voluntariamente decida no continuar, deber&aacute; manifestarlo entonces
                        por escrito. b) </span><b>El CONTRATANTE </b><span style="font-weight: 400;">deber&aacute;
                        manifestar por escrito su intenci&oacute;n de contrataci&oacute;n directa del recurso,&nbsp;</span>
                </li>
            </ol>
            <p><span style="font-weight: 400;">con </span><b>UN (1) MES </b><span style="font-weight: 400;">de
                    anticipaci&oacute;n. c) En caso que </span><b>EL CONTRATANTE </b><span style="font-weight: 400;">inicie
                    una contrataci&oacute;n directa con el recurso asignado, este deber&aacute; pagar adicional al
                </span><b>CONTRATISTA</b><span style="font-weight: 400;">, el valor del </span><b>100% de UN (1) </b><span
                    style="font-weight: 400;">Salario mensual , del perfil contratado, correspondiente al </span><b>EXIT
                </b><span style="font-weight: 400;">del recurso.&nbsp;</span></p>
            <p><b>CL&Aacute;USULA OCTAVA: DERECHOS DEL CONTRATISTA&nbsp;</b></p>
            <ul>
                <li><span style="font-weight: 400;"> </span><span style="font-weight: 400;">Recibir la remuneraci&oacute;n
                        correspondiente en los t&eacute;rminos pactados en la cl&aacute;usula cuarta del presente
                        Contrato.&nbsp;</span></li>
                <li><span style="font-weight: 400;"> </span><span style="font-weight: 400;">Recibir toda la
                        informaci&oacute;n necesaria y adecuada para el cabal cumplimiento del presente
                        contrato.&nbsp;</span></li>
                <li><span style="font-weight: 400;"> </span><span style="font-weight: 400;">Solicitar en cualquier etapa del
                        proceso y antes de la firma de cualquier documento, aclaraciones, rectificaciones, correcciones,
                        modificaciones, sobre cualquier aspecto relacionado con el presente contrato, las cuales, en caso de
                        efectuarse, deber&aacute;n constar por escrito.&nbsp;</span></li>
            </ul>
            <p><b>CL&Aacute;USULA NOVENA: OBLIGACIONES GENERALES DEL CONTRATISTA&nbsp;</b></p>
            <p><b>EL CONTRATISTA </b><span style="font-weight: 400;">se obliga a ejecutar el objeto del contrato y a
                    desarrollar las actividades espec&iacute;ficas en las condiciones pactadas.&nbsp;</span></p>
            <p><b>EL CONTRATISTA </b><span style="font-weight: 400;">debe custodiar y a la terminaci&oacute;n del presente
                    contrato devolver los insumos, suministros, herramientas, documentos, inventarios y/o materiales que le
                    sean puestos a su disposici&oacute;n para la prestaci&oacute;n del servicio objeto de este contrato.
                    Igualmente deber&aacute; eliminar o volver ilegible toda la informaci&oacute;n que reciba a
                    trav&eacute;s de medios electr&oacute;nicos, y que haya sido puesta a su disposici&oacute;n para la
                    prestaci&oacute;n del servicio objeto de este contrato.&nbsp;</span></p>
            <p><span style="font-weight: 400;">En caso de retiro del recurso </span><b>EL CONTRATISTA </b><span
                    style="font-weight: 400;">ofrecer&aacute; a </span><b>EL CONTRATANTE </b><span
                    style="font-weight: 400;">en las mismas o mejores condiciones, otras alternativas de recursos para que
                </span><b>EL CONTRATANTE </b><span style="font-weight: 400;">decida y escoja de las alternativas propuestas,
                    la que mejor se ajuste a sus necesidades.&nbsp;</span></p>
            <p><span style="font-weight: 400;">En los eventos en los que el recurso dispuesto por </span><b>EL CONTRATISTA
                </b><span style="font-weight: 400;">no decida continuar y no exista disponibilidad inmediata de nuevos
                    recursos, se har&aacute; entrega de las tareas hasta tal fecha, se facturar&aacute; hasta esta fecha y
                    cesa la responsabilidad por parte de </span><b>EL CONTRATISTA </b><span style="font-weight: 400;">de
                    proveer recursos a </span><b>EL CONTRATANTE </b><span style="font-weight: 400;">hasta el momento en que
                    exista disponibilidad de nuevos recursos.&nbsp;</span></p>
            <p><span style="font-weight: 400;">Es responsabilidad del El CONTRATANTE soportar mediante alguna herramienta de
                    seguimiento, la ejecuci&oacute;n de dichas tareas.</span></p>
            <p style="text-align: center;"></p>
            <p><b>CL&Aacute;USULA D&Eacute;CIMA: DERECHOS DEL CONTRATANTE&nbsp;</b></p>
            <ul>
                <li><span style="font-weight: 400;"> </span><span style="font-weight: 400;">Solicitar en cualquier etapa del
                        proceso al </span><b>CONTRATISTA </b><span style="font-weight: 400;">de manera verbal y/o escrita,
                        informaci&oacute;n referente al desarrollo del proyecto, y a sus avances.&nbsp;</span></li>
                <li><span style="font-weight: 400;"> </span><span style="font-weight: 400;">Realizar aclaraciones,
                        correcciones, sugerencias y dem&aacute;s al </span><b>CONTRATISTA, </b><span
                        style="font-weight: 400;">siempre y cuando las mismas no afecten de manera sustancial la labor
                        ejecutada o modifiquen aspectos propios del contrato, o que afecten de manera significativa el
                        desarrollo del mismo.&nbsp;</span></li>
            </ul>
            <p><b>CL&Aacute;USULA D&Eacute;CIMA PRIMERA: OBLIGACIONES GENERALES DEL CONTRATANTE&nbsp;</b></p>
            <ul>
                <li><span style="font-weight: 400;"> </span><span style="font-weight: 400;">Cumplir de manera adecuada con
                        el objetivo del contrato en cuanto a suministro de informaci&oacute;n, requerimientos del
                    </span><b>CONTRATISTA </b><span style="font-weight: 400;">para el adecuado cumplimiento del mismo,
                        suministros, equipos, y dem&aacute;s que se requiera para que el </span><b>CONTRATISTA </b><span
                        style="font-weight: 400;">pueda cumplir adecuadamente con el objeto del contrato.&nbsp;</span></li>
                <li><span style="font-weight: 400;"> </span><span style="font-weight: 400;">Ejercer el respectivo control en
                        el cumplimiento del objeto del contrato y expedir el documento de cumplimiento a
                        satisfacci&oacute;n.&nbsp;</span></li>
                <li><span style="font-weight: 400;"> </span><span style="font-weight: 400;">Pagar el valor del contrato de
                        acuerdo con los t&eacute;rminos y fechas establecidas.&nbsp;</span></li>
                <li><span style="font-weight: 400;"> </span><span style="font-weight: 400;">Prestar su colaboraci&oacute;n
                        para el cumplimiento de las obligaciones del </span><b>CONTRATISTA.&nbsp;</b></li>
            </ul>
            <p><b>CL&Aacute;USULA D&Eacute;CIMA SEGUNDA: RESPONSABILIDAD E INDEMNIDAD&nbsp;</b></p>
            <p><b>EL CONTRATISTA </b><span style="font-weight: 400;">es responsable por el cumplimiento del objeto del
                    contrato dentro de las fechas establecidas y con los requerimientos previamente acordados entre las
                    partes.&nbsp;</span></p>
            <p><span style="font-weight: 400;">En igual sentido, </span><b>EL CONTRATANTE </b><span
                    style="font-weight: 400;">es responsable en caso de incumplimiento de sus obligaciones para con el
                </span><b>CONTRATISTA </b><span style="font-weight: 400;">en el desarrollo y ejecuci&oacute;n del presente
                    contrato.&nbsp;</span></p>
            <p><b>NINGUNA DE LAS PARTES </b><span style="font-weight: 400;">ser&aacute; responsable frente a la otra o
                    frente a terceros por da&ntilde;os especiales, imprevisibles o da&ntilde;os indirectos, derivados de
                    fuerza mayor, caso fortuito o circunstancias especiales atribuibles a terceros y ajenas a la labor
                    principal de cada una de las partes en el desarrollo del objeto del presente contrato.&nbsp;</span></p>
            <p><b>EL CONTRATANTE </b><span style="font-weight: 400;">mantendr&aacute; indemne a </span><b>EL CONTRATISTA
                </b><span style="font-weight: 400;">de cualquier reclamaci&oacute;n por circunstancias, fallas e imprevistos
                    que puedan presentarse, que no sean de responsabilidad de </span><b>EL CONTRATISTA </b><span
                    style="font-weight: 400;">y que sean atribuibles a terceros en el desarrollo del objeto del presente
                    contrato.&nbsp;</span></p>
            <p><b>CL&Aacute;USULA D&Eacute;CIMA TERCERA: TERMINACI&Oacute;N&nbsp;</b></p>
            <p><span style="font-weight: 400;">El presente contrato podr&aacute; darse por terminado por mutuo acuerdo entre
                    las partes, o en forma unilateral por el incumplimiento de las obligaciones derivadas del contrato, por
                    cualquiera de ellas.&nbsp;</span></p>
            <ol>
                <li><b> POR MUTUO ACUERDO. </b><span style="font-weight: 400;">Por mutuo acuerdo las partes podr&aacute;n
                        dar por terminado este contrato en&nbsp;</span></li>
            </ol>
            <p><span style="font-weight: 400;">cualquier tiempo, expresada en el r&eacute;gimen de terminaci&oacute;n que
                    contiene el presente contrato.&nbsp;</span></p>
            <ol>
                <li><b> DE PLENO DERECHO. </b><span style="font-weight: 400;">Las partes podr&aacute;n dar por terminado el
                        contrato en cualquier momento y sin previo aviso, cuando una de las partes haya incurrido en el
                        incumplimiento de cualquiera de las obligaciones pactadas en este documento, sin que medie caso
                        fortuito o fuerza mayor.&nbsp;</span></li>
                <li><b> POR JUSTA CAUSA</b><b>. </b><span style="font-weight: 400;">Las partes podr&aacute;n dar por
                        terminado el presente contrato, sin generar&nbsp; indemnizaci&oacute;n por las siguientes
                        razones:&nbsp;</span></li>
                <li><span style="font-weight: 400;"> Por violaci&oacute;n del derecho de reserva sobre la informaci&oacute;n
                        especialmente al uso personal o de terceros de sus manuales operativos, instrucciones.&nbsp;</span>
                </li>
                <li><span style="font-weight: 400;"> Por el incumplimiento a cualquiera de los compromisos econ&oacute;micos
                        que conllevan para las&nbsp; partes el presente contrato.&nbsp;</span></li>
                <li><b> POR EFECTOS LEGALES. </b><span style="font-weight: 400;">En caso de vulneraci&oacute;n a las
                        disposiciones legales que actualmente o hacia futuro rigen el presente contrato, que hagan imposible
                        para las partes continuar con la ejecuci&oacute;n del mismo. En este caso no habr&aacute; lugar al
                        pago de indemnizaci&oacute;n.&nbsp;</span></li>
                <li><b> POR SENTENCIA JUDICIAL </b><span style="font-weight: 400;">que as&iacute; lo ordene.&nbsp;</span></li>
            </ol>
            <p><span style="font-weight: 400;">La cesi&oacute;n o cualquier acto unilateral de disposici&oacute;n de alguna
                    de las partes sobre el contrato, constituye causal de terminaci&oacute;n unilateral del contrato con
                    indemnizaci&oacute;n por la suma de diez (10) salarios m&iacute;nimos legales vigentes mensuales en
                    favor de la parte afectada.&nbsp;</span></p>
            <p><b>CL&Aacute;USULA D&Eacute;CIMA CUARTA: EXCLUSI&Oacute;N DE LA RELACI&Oacute;N LABORAL&nbsp;</b></p>
            <p><b>EL CONTRATISTA </b><span style="font-weight: 400;">actuar&aacute; por su propia cuenta, con absoluta
                    autonom&iacute;a y no estar&aacute; sometido a subordinaci&oacute;n laboral con </span><b>EL CONTRATANTE
                </b><span style="font-weight: 400;">y sus derechos se limitar&aacute;n, de acuerdo con la naturaleza del
                    contrato, a exigir el cumplimiento de las obligaciones del </span><b>CONTRATISTA </b><span
                    style="font-weight: 400;">y al pago de los honorarios estipulados por la prestaci&oacute;n del servicio.
                    Queda claramente entendido que no existir&aacute; relaci&oacute;n laboral alguna entre </span><b>EL
                    CONTRATANTE y EL CONTRATISTA, </b><span style="font-weight: 400;">o el personal que &eacute;stos
                    utilicen en la ejecuci&oacute;n del objeto del presente contrato.&nbsp;</span></p>
            <p><b>CL&Aacute;USULA D&Eacute;CIMA QUINTA: CL&Aacute;USULA PENAL&nbsp;</b></p>
            <p><span style="font-weight: 400;">En caso de incumplimiento total o parcial de las obligaciones del presente
                    Contrato por cualquiera de las partes, la parte incumplida, deber&aacute; pagar a la parte cumplida a
                    t&iacute;tulo de indemnizaci&oacute;n, una suma equivalente al </span><b>DIEZ POR CIENTO (10%) </b><span
                    style="font-weight: 400;">del valor total del contrato.&nbsp;</span></p>
            <p><span style="font-weight: 400;">El valor pactado de la presente cl&aacute;usula penal es el de la
                    estimaci&oacute;n anticipada de perjuicios. En armon&iacute;a con lo dispuesto por los 1602, y 1546 del
                    C&oacute;digo Civil, en los contratos bilaterales, si uno de los contratantes no cumple lo pactado,
                    opera la condici&oacute;n resolutoria y, en tal caso, por ministerio de la ley se faculta al
                    otro&nbsp;</span></p>
            <p><span style="font-weight: 400;">contratante para pedir a su arbitrio, o el cumplimiento del contrato o su
                    resoluci&oacute;n, en ambos casos con la indemnizaci&oacute;n de perjuicios
                    correspondiente.&nbsp;</span></p>
            <p><b>CL&Aacute;USULA D&Eacute;CIMA SEXTA: PROPIEDAD INTELECTUAL E INDUSTRIAL.&nbsp;</b></p>
            <p><span style="font-weight: 400;">Los derechos y el manejo de cada una de las partes sobre la propiedad
                    intelectual e industrial se regular&aacute;n de acuerdo con los siguientes
                    par&aacute;metros:&nbsp;</span></p>
            <ul>
                <li><span style="font-weight: 400;"> </span><span style="font-weight: 400;">De la propiedad intelectual que
                        surja del desarrollo del presente contrato, el </span><b>CONTRATISTA </b><span
                        style="font-weight: 400;">ser&aacute; titular de los derechos morales y </span><b>EL CONTRATANTE
                    </b><span style="font-weight: 400;">de los derechos patrimoniales, al igual que el derecho a utilizar su
                        contenido.&nbsp;</span></li>
                <li><span style="font-weight: 400;"> </span><span style="font-weight: 400;">Los derechos de propiedad sobre
                        las marcas, nombres, logos y emblemas tanto del </span><b>CONTRATANTE </b><span
                        style="font-weight: 400;">como del </span><b>CONTRATISTA </b><span style="font-weight: 400;">son de
                        propiedad exclusiva de cada una de ellas, Por lo tanto, ninguna de las partes podr&aacute; utilizar
                        la marca, el nombre, el logo y el emblema o cualquier otro signo que lo distinga sin que exista
                        autorizaci&oacute;n previa de la otra parte, y su utilizaci&oacute;n no constituir&aacute; en
                        ning&uacute;n sentido derechos de propiedad intelectual sobre los mismos.&nbsp;</span></li>
            </ul>
            <p><b>CL&Aacute;USULA D&Eacute;CIMA S&Eacute;PTIMA. CONFIDENCIALIDAD Y MANEJO DE LA
                    INFORMACI&Oacute;N.&nbsp;</b></p>
            <p><span style="font-weight: 400;">Las partes, sus empleados y/o personal se abstendr&aacute;n de divulgar,
                    publicar, comunicar directa o indirectamente a terceros o utilizar para prop&oacute;sitos diferentes a
                    la correcta ejecuci&oacute;n de este contrato, la informaci&oacute;n relacionada con la ejecuci&oacute;n
                    del mismo, por lo que convienen expresamente que toda informaci&oacute;n a la que tengan acceso o
                    reciban en virtud del presente se considera confidencial y por lo tanto divulgar o transmitir la
                    informaci&oacute;n considerada como tal, dar&aacute; derecho a la parte cumplida a ejercer las acciones
                    legales correspondientes. La obligaci&oacute;n de confidencialidad a cargo de las partes y consagrada en
                    la presente cl&aacute;usula, subsistir&aacute; aun despu&eacute;s de la terminaci&oacute;n del presente
                    contrato.&nbsp;</span></p>
            <p><b>CL&Aacute;USULA D&Eacute;CIMA OCTAVA: PR&Oacute;RROGA&nbsp;</b></p>
            <p><span style="font-weight: 400;">Si vencido el plazo establecido para la ejecuci&oacute;n del contrato de
                    prestaci&oacute;n de servicios el </span><b>CONTRATANTE </b><span style="font-weight: 400;">decide
                    ampliar el plazo de vencimiento, se elaborar&aacute; un otros&iacute; en tal sentido, el cual
                    har&aacute; parte integral de este contrato.&nbsp;</span></p>
            <p><b>CL&Aacute;USULA D&Eacute;CIMA NOVENA: CESIONES&nbsp;</b></p>
            <p><b>EL CONTRATISTA </b><span style="font-weight: 400;">no puede ceder parcial ni totalmente sus obligaciones o
                    derechos derivados del presente contrato sin la autorizaci&oacute;n previa, expresa y escrita del
                </span><b>CONTRATANTE, </b><span style="font-weight: 400;">y en igual sentido con el
                </span><b>CONTRATANTE.&nbsp;</b></p>
            <p><b>CL&Aacute;USULA VIG&Eacute;SIMA: CASO FORTUITO O FUERZA MAYOR:&nbsp;</b></p>
            <p><span style="font-weight: 400;">Las partes quedan exoneradas de responsabilidad por el incumplimiento de
                    cualquiera de sus obligaciones o por la demora en la satisfacci&oacute;n de cualquiera de las
                    prestaciones a su cargo derivadas del presente contrato, cuando el incumplimiento sea resultado o
                    consecuencia de la ocurrencia de un evento de fuerza mayor y caso fortuito debidamente invocadas y
                    constatadas de acuerdo con la ley y la jurisprudencia colombiana.&nbsp;</span></p>
            <p><b>CL&Aacute;USULA VIG&Eacute;SIMA PRIMERA: NUEVO SERVICIO&nbsp;</b></p>
            <p><span style="font-weight: 400;">Si finalizado el objeto del servicio contratado, el </span><b>CONTRATANTE
                </b><span style="font-weight: 400;">necesita un nuevo servicio del </span><b>CONTRATISTA, </b><span
                    style="font-weight: 400;">se deber&aacute; hacer un nuevo Contrato de Prestaci&oacute;n de Servicios y
                    no se entender&aacute; como pr&oacute;rroga por desaparecer las causas contractuales que dieron origen
                    al presente contrato.&nbsp;</span></p>
            <p><b>CL&Aacute;USULA VIG&Eacute;SIMA SEGUNDA: SOLUCI&Oacute;N DE CONTROVERSIAS:&nbsp;</b></p>
            <p><span style="font-weight: 400;">Las controversias o diferencias que surjan entre el </span><b>CONTRATISTA
                </b><span style="font-weight: 400;">y el </span><b>CONTRATANTE </b><span style="font-weight: 400;">con
                    ocasi&oacute;n de la firma, ejecuci&oacute;n, interpretaci&oacute;n, pr&oacute;rroga o
                    terminaci&oacute;n del contrato, as&iacute; como de cualquier otro asunto relacionado con el presente
                    contrato, se intentar&aacute; buscar en primera instancia un arreglo directo, el cual deber&aacute;
                    efectuarse en un t&eacute;rmino no mayor a cinco (5) d&iacute;as h&aacute;biles a partir de la fecha en
                    que cualquiera de las partes comunique por escrito a la otra la existencia de una
                    diferencia.&nbsp;</span></p>
            <p><span style="font-weight: 400;">Cuando la controversia no pueda solucionarse de manera directa por las partes
                    involucradas luego de un periodo de </span><b>TREINTA (30) </b><span
                    style="font-weight: 400;">d&iacute;as h&aacute;biles contados desde que una de las partes pone en
                    conocimiento de la otra la respectiva controversia, el asunto debe someterse a un procedimiento
                    conciliatorio que se surtir&aacute; ante un Centro de Conciliaci&oacute;n debidamente autorizado por la
                    ley para tal fin. Si en el t&eacute;rmino de ocho (8) d&iacute;as h&aacute;biles a partir del inicio del
                    tr&aacute;mite de la conciliaci&oacute;n, el cual se entender&aacute; a partir de la fecha de la primera
                    citaci&oacute;n a las Partes, estas no llegan a un acuerdo para resolver sus diferencias, las mismas
                    deber&aacute;n acudir como &uacute;ltima y definitiva instancia a la jurisdicci&oacute;n
                    ordinaria.&nbsp;</span></p>
            <p><span style="font-weight: 400;">En todo caso, este contrato presta m&eacute;rito ejecutivo por ser una
                    obligaci&oacute;n clara, expresa y exigible para las partes.&nbsp;</span></p>
            <p><b>CL&Aacute;USULA VIG&Eacute;SIMA TERCERA: ANEXOS DEL CONTRATO&nbsp;</b></p>
            <p><span style="font-weight: 400;">Hacen parte integrante de este contrato los siguientes
                    documentos:&nbsp;</span></p>
            <ul>
                <li><span style="font-weight: 400;"> Certificados de existencia y representaci&oacute;n legal de cada una de
                        las partes.&nbsp;</span></li>
                <li><span style="font-weight: 400;"> Aprobaci&oacute;n por correo u otro medio de la tarifa mensual del
                        recurso asignado.&nbsp;</span></li>
            </ul>
            <p><b>CL&Aacute;USULA VIG&Eacute;SIMA CUARTA: LEGISLACI&Oacute;N APLICABLE: </b><span
                    style="font-weight: 400;">El presente contrato se rige por las leyes de la Rep&uacute;blica de Colombia,
                    en especial por las normas de derecho privado aplicables, es decir, el C&oacute;digo Civil y
                    C&oacute;digo de Comercio y las dem&aacute;s disposiciones aplicables al caso en concreto.&nbsp;</span>
            </p>
            <p><b>CL&Aacute;USULA VIG&Eacute;SIMA SEXTA: DOMICILIO CONTRACTUAL:&nbsp;</b></p>
            <p><span style="font-weight: 400;">El domicilio contractual es para todos los efectos legales la ciudad de
                    Bogot&aacute; D.C&nbsp;</span></p>
`
        }
    }
}