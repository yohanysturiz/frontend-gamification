export const myMixins = {
    methods: {

        printDateRequest(element) {
            let dateFormat = new Date(element);
            var months = [
                "01",
                "02",
                "03",
                "04",
                "05",
                "06",
                "07",
                "08",
                "09",
                "10",
                "11",
                "12"
            ];

            return `${dateFormat.getDate()} / ${
                months[dateFormat.getMonth()]
            } / ${dateFormat.getFullYear()}`;
        },

        updatePaymentAccountStatus(value) {
            switch (value) {
            case "0":
                status = "Pendiente";
                break;
            case "1":
                status = "En proceso";
                break;
            case "2":
                status = "Pagado";
                break;
            case "3":
                status = "Negado";
                break;

            default:
                status = "failed";
                break;
            }
            return status;
        },

        capitalize(value) {
            return value.charAt(0).toUpperCase() + value.slice(1)
        },

        replaceString(value, find, replace) {
            let newValue = value.split(find);
            return newValue.join(" ");
        },

        firmDescription(bol) {
            if (bol) return "Firmado"
            else return "Por firmar"
        },

        getClient(val) {
            if (val == null) return "IMUKO"
            else return val.company_name
        },

        typeContract(val) {
            var type = "n/a"
            switch (val) {
            case 0:
                type = "Indefinido"
                break;
            case 1:
                type = "Servicios"
                break;
            default:
                type = "Servicios"
                break;
            }
            return type;
        },

        printStatus(value) {
            var color = "n/a"
            switch (value) {
            case 'aprobado':
                color = "success"
                break;
            case 'pendiente':
                color = "warning"
                break;
            case 'vencido':
                color = "danger"
                break;
            default:
                color = "info"
                break;

            }
            return color;
        },

        validate_letters_field(rule, value, callback) {
            var letters = `%&#*/^(([^<0123456789''>()\[\]\\.,;:\@"]+(\.[^<>()\[\]\\.,;:\@"]+)*)|(".+"))@((\[\])|(([\-]+\.)+[-]{,}))$/`;
            for (var i of value) {
                if (letters.includes(i)) callback(new Error());
            }
            callback();
        },

        validate_number_field(rule, value, callback) {
            value = value.toString()
            var letters = `%&/^(([^<>()\[\]\\,;!¡¿?={#*}:\@"]+(\[^'<>()\[\]\\,;:\@"]+[aAbBcCdDeEfFgGhHiIjJkKlLmMnNñÑoOpPqQrRsStTuUvVwWxXyYzZ])*)|("+"))@((\[\])|(([\-]+\)+[-]{,}))$/`;
            for (var i of value) {
                if (letters.includes(i)) callback(new Error());
            }
            callback();
        },

        getRandom() {
            return Math.round(Math.random() * 100000);
        },

        printrequestStatus(statusValue) {
            switch (statusValue) {
            case 1:
                statusValue = "Activo";
                break;
            case 2:
                statusValue = "Pendiente";
                break;
            case 3:
                statusValue = "Selección";
                break;
            default:
                statusValue = "Inactivo";
                break;
            }
            return statusValue;
        },

        loadingButtonAnimation(value) {
            if (value) {
                this.loadingButton = true
            }
            return this.loadingButton
        },

        sendWappMessage(phone, msg) {
            var phoneNumber = "+57" + phone
            this.link.href = 'https://api.whatsapp.com/send?phone=' + phoneNumber + '&text=' + msg;
            this.link.target = "_blank";
            this.body.appendChild(this.link);
            this.link.click();
        },
        getPeriodicity(periodicity, period) {
            const monthly_periodicity = [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ];

            const quartely_periodicity = [
                'January - March',
                'April - June',
                'July - September',
                'Octuber - December'
            ];

            const semiannual_periodicity = [
                'January - March',
                'April - June',
                'July - September',
                'Octuber - December'
            ];

            switch (periodicity) {
                case 1:
                    return monthly_periodicity[period-1];
                case 2:
                    return quartely_periodicity[period-1];
                case 3:
                    return semiannual_periodicity[period-1];
                default:
                    return period;
            }
        }
    }
}

