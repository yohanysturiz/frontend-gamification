export const myContracts = {
    data: function() {
        return {
        }
    },
    methods: {
        getContractCandidate(contract_data){
            return `<p style="text-align: justify;"><strong>CONTRATO DE PRESTACIÓN DE SERVICIOS PROFESIONALES, CELEBRADO ENTRE IMUKO SAS Y $full_name</strong>
        </p>
        <p style="text-align: justify;"><strong>&nbsp;</strong></p>
        <div class="body" style="text-align: justify;">
        <p style="text-align: justify;">Entre los suscritos <strong>TATIANA PRIETO TRIANA, </strong>identificada con Cédula de ciudadanía N&ordm; 52.981.604 expedida en Bogotá D.C. domiciliado y residente en la
            ciudad de Bogotá D.C, actuando en su calidad de Representante Legal de <strong>IMUKO S.A.S</strong> sociedad identificada con el NIT 901250358 - 1 como consta en el Certificado de Existencia y
            Representación Legal expedido por la Cámara de Comercio de Bogotá D.C quien para los efectos del presente contrato se denominará <strong>EL CONTRATANTE</strong>, y por la otra
            <strong> $full_name </strong>
            identificado con <strong> $type_document </strong> No. <strong> $number_document </strong> expedida en
            <strong> $city_expedition_doc </strong>,
             domiciliado y residente en la ciudad de <strong> $city_resident </strong>, actuando en nombre propio y quien
            para los efectos del presente contrato se denominará<strong>, EL CONTRATISTA, </strong>por una parte; y
            por la otra, hemos convenido en celebrar el presente<strong> CONTRATO DE PRESTACIÓN DE SERVICIOS
                PROFESIONALES,</strong> teniendo en cuenta las siguientes</p>
        <p style="text-align: justify;">&nbsp;</p>
        <p style="text-align: center;"><strong>CONSIDERACIONES </strong></p>
        <p style="text-align: justify;">&nbsp;</p>
        <ol style="text-align: justify;">
            <li>Que <strong>EL CONTRATANTE </strong>en su labor de Desarrollador de Software  en las tecnologías $lenguage_programming tiene la capacidad operativa suficiente para el desarrollo del objeto del presente contrato. </li>
            <li>Que <strong>EL CONTRATANTE </strong>requiere la prestación de servicios de portafolio del <strong>CONTRATISTA.</strong> </li>
            <li>Que, para el desarrollo del objeto del presente contrato, <strong>EL CONTRATANTE</strong> requiere entonces de los servicios que prestará <strong>EL CONTRATISTA</strong> </li>
            <li>Que <strong>EL CONTRATISTA</strong> actuará por su propia cuenta, con absoluta autonomía y no estará sometido a subordinación laboral con <strong>EL CONTRATANTE</strong> y sus derechos se limitarán, 
                de acuerdo con la naturaleza del contrato, únicamente a exigir el pago estipulados por la prestación del servicio mientras que <strong>EL CONTRATANTE</strong> se limitará a exigir el cumplimiento del objeto del contrato de conformidad con las especificaciones jurídicas y técnicas señaladas en el presente documento, sin ningún otro tipo de exigencia adicional. Adicionalmente <strong>EL CONTRATISTA</strong> entiende y acepta que para el pago de sus servicios profesionales deberá acreditarle al <strong>CONTRATANTE</strong> el pago de su Seguridad Social en las fechas establecidas para tal efecto, para lo cual <strong>EL CONTRATISTA</strong> se compromete a efectuar estos aportes al Sistema General de Seguridad Social como profesional independiente, razón por la que <strong>NO</strong> existe ningún tipo de obligación sobre el particular a cargo del <strong>CONTRATANTE.</strong></li>
        </ol>
        <p style="text-align: justify;">Por lo anterior, las partes celebran el presente contrato, el cual se regirá
            por las siguientes cláusulas:</p>
        <p style="text-align: justify;">&nbsp;</p>
        <p style="text-align: justify;"><strong>PRIMERA: &nbsp;OBJETO </strong></p>
        <p style="text-align: justify;">El <strong>OBJETO GENERAL </strong>del contrato consiste en ejecutar tareas de
            desarrollo del <strong>CONTRATISTA </strong>hacia el <strong>CONTRATANTE</strong> según los
            requerimientos de carácter técnico que este formule, utilizando sus propios medios, elementos de trabajo, experiencia y conocimiento, <strong>EL CONTRATISTA</strong> prestará a favor de lo proyectos de IMUKO servicios de desarrollo y pruebas de programas de software en lenguaje $lenguage_programming de conformidad con los requerimientos de carácter técnico del <strong>CONTRATANTE</strong> hacia <strong>EL CONTRATISTA</strong>, para sus diferentes proyectos</p>
        <p style="text-align: justify;"><strong>SEGUNDA: &nbsp;ACTIVIDADES ESPECÍFICAS DEL CONTRATO</strong></p>
        <p style="text-align: justify;">Las actividades específicas a desarrollar para la prestación de los
            servicios profesionales, de conformidad con los requerimientos efectuados por el solicitante serán las
            siguientes:</p>
        <ul style="text-align: justify;">
            <li>Ejecución de actividades de desarrollo de software, de conformidad con los requerimientos y objetivos del <strong>CONTRATANTE</strong> hacia <strong>EL CONTRATISTA</strong>.  Iniciando actividades el día <strong>$start_date</strong></li>
        </ul>
        <p style="text-align: justify;">&nbsp;</p>
        <p style="text-align: justify;"><strong>TERCERA: ENTREGABLES </strong><strong>&nbsp;</strong></p>
        <p style="text-align: justify;">En desarrollo de las cláusulas Primera y Segunda del presente contrato,
            <strong>EL CONTRATISTA</strong> deberá presentar los entregables en los que dé cuenta de las
            actuaciones realizadas.</p>
        <p style="text-align: justify;">Estos deberán responder a los requerimientos efectuados por el solicitante de
            conformidad con las especificaciones técnicas correspondientes.</p>
        <p style="text-align: justify;">&nbsp;</p>
        <p style="text-align: justify;"><strong>CUARTA. DURACIÓN</strong>&nbsp;
        </p>
        <p style="text-align: justify;">El plazo del presente contrato tendrá una duración de <strong>$contract_duration</strong> contados a partir de  la fecha de inicio descrita en este documento y acordada previamente entre las partes. 
        Este plazo inicial puede ser ampliado de común acuerdo entre las partes si las mismas luego de finalizado el periodo inicial y por necesidades propias del objeto del contrato, requieren efectuar tal ampliación del periodo, la cual deberá constar en un documento anexo al presente contrato firmado por ambas partes.
        </p>

        <p style="text-align: justify;">Igualmente, el <strong>CONTRATANTE</strong> está facultado para dar por terminado el presente contrato con <strong>EL CONTRATISTA</strong> DE MANERA ANTICIPADA, si observa que este último incumplió con alguno de sus deberes, o con alguna de las cláusulas de este contrato, evento en el cual el <strong>CONTRATANTE</strong> cancelará los dineros que se adeuden hasta la fecha en que se haga efectiva la terminación unilateral del contrato.
        </p>

        <p style="text-align: justify;"><strong>QUINTA: VALOR CONTRATO Y FORMA DE PAGO</strong>&nbsp;
        </p>
        <p style="text-align: justify;">El valor mensual del presente contrato corresponderá a la suma de
            <strong> $contract_value_letter ($ $contract_value_number) </strong> el pago se efectuará de manera mensual, durante los 2 días hábiles después de radicada la cuenta de cobro, junto con los documentos solicitados, en la plataforma de IMUKO, este pago se realizará hasta completar el objeto del contrato.</p>
        <p style="text-align: justify;">&nbsp;</p>
        <p style="text-align: justify;"><strong>SEXTA: INICIO DEL CONTRATO</strong>&nbsp;</p>
        <p style="text-align: justify;">El contrato se dará por iniciado, a partir del <strong>$start_date</strong> previa aprobación y firma de este documento.</p>
        <p style="text-align: justify;">&nbsp;</p>
        <p style="text-align: justify;"><strong>SEPTIMA: &nbsp;DECLARACIONES DEL CONTRATISTA</strong></p>
        <p style="text-align: justify;"><strong>EL CONTRATISTA </strong>hace las siguientes declaraciones:</p>
        <ul style="text-align: justify;">
            <li>Conoce y acepta los Documentos del Proceso</li>
            <li>Tuvo la oportunidad de solicitar aclaraciones y modificaciones a los mismos, y recibió del
                <strong>CONTRATANTE</strong> respuesta oportuna a cada una de las solicitudes.</li>
            <li>Se encuentra debidamente facultado para suscribir el presente Contrato</li>
        </ul>
        <p style="text-align: justify;"><strong>OCTAVA: DERECHOS DEL CONTRATISTA</strong></p>
        <ul style="text-align: justify;">
            <li>Recibir la remuneración correspondiente en los términos pactados en la cláusula cuarta
                del presente Contrato.</li>
            <li>Recibir toda la información necesaria y adecuada para el cabal cumplimiento del presente contrato.
            </li>
            <li>Solicitar en cualquier etapa del proceso y antes de la firma de cualquier documento, aclaraciones,
                rectificaciones, correcciones, modificaciones, sobre cualquier aspecto relacionado con el presente contrato,
                las cuales, en caso de efectuarse, deberán constar por escrito.</li>
        </ul>
        <p style="text-align: justify;"><strong>NOVENA: OBLIGACIONES GENERALES DEL CONTRATISTA</strong></p>
        <ul style="text-align: justify;">
            <li><strong>EL CONTRATISTA</strong> se obliga a ejecutar el objeto del contrato y a desarrollar las actividades
                específicas en las condiciones pactadas.</li>
            <li><strong>EL CONTRATISTA </strong>debe custodiar y a la terminación del presente contrato devolver los
                insumos, suministros, herramientas, documentos, inventarios y/o materiales que le sean puestos a su
                disposición para la prestación del servicio objeto de este contrato. Igualmente deberá
                eliminar o volver ilegible toda la información que reciba a través de medios
                electrónicos, y que haya sido puesta a su disposición para la prestación del servicio
                objeto de este contrato.</li>
            <li><strong>El CONTRATISTA</strong> deberá acreditar los correspondientes pagos a los Sistemas de
                Seguridad Social como profesional independiente, o en su defecto <strong>EL CONTRATANTE</strong>
                estará facultado para efectuar las correspondientes retenciones de manera directa y reportar los
                pagos correspondientes para acreditar el cumplimiento mensual de dichos pagos ante el Sistema de Seguridad
                Social Integral. En consecuencia,<strong> NO</strong> existe ninguna obligación de parte del
                <strong>CONTRATANTE, </strong>al ser una responsabilidad integral del <strong>CONTRATISTA</strong> efectuar
                sus aportes al Sistema de Seguridad Social dentro de las fechas establecidas, y como requisito esencial para
                el pago de sus honorarios por los servicios prestados.</li>
        </ul>
    
        <p style="text-align: justify;"><strong>DÉCIMA: &nbsp;DERECHOS DEL CONTRATANTE </strong></p>
        <ul style="text-align: justify;">
            <li>Solicitar en cualquier etapa del proceso al <strong>CONTRATISTA</strong> de manera verbal y/o escrita,
                información referente al desarrollo del proyecto, y a sus avances.</li>
            <li>Realizar aclaraciones, correcciones, sugerencias y demás al <strong>CONTRATISTA, </strong>siempre y
                cuando las mismas no afecten de manera sustancial la labor ejecutada o modifiquen aspectos propios del
                contrato, o que afecten de manera significativa el desarrollo del mismo.</li>
        </ul>
        <p style="text-align: justify;"><br /> <br /> <strong>DÉCIMA PRIMERA: OBLIGACIONES GENERALES DEL CONTRATANTE
            </strong></p>
        <ul style="text-align: justify;">
            <li>Cumplir de manera adecuada con el objetivo del contrato en cuanto a suministro de información,
                requerimientos del <strong>CONTRATISTA</strong> para el adecuado cumplimiento del mismo, suministros,
                equipos, y demás que se requiera para que el <strong>CONTRATISTA</strong> pueda cumplir adecuadamente
                con el objeto del contrato.</li>
        </ul>
        <ul style="text-align: justify;">
            <li>Ejercer el respectivo control en el cumplimiento del objeto del contrato y expedir el documento de
                cumplimiento a satisfacción.</li>
        </ul>
        <ul style="text-align: justify;">
            <li>Pagar el valor del contrato de acuerdo con los términos y fechas establecidas.</li>
        </ul>
        <ul style="text-align: justify;">
            <li>Prestar su colaboración para el cumplimiento de las obligaciones del <strong>CONTRATISTA</strong></li>
        </ul>
    
        <p style="text-align: justify;"><strong>DÉCIMA SEGUNDA: &nbsp;RESPONSABILIDAD E INDEMNIDAD </strong></p>
    
        <p style="text-align: justify;"><strong>EL CONTRATISTA </strong>es responsable por el cumplimiento del objeto del
            contrato dentro de las fechas establecidas y con los requerimientos previamente acordados entre las partes.</p>
        <p style="text-align: justify;">En igual sentido, <strong>EL CONTRATANTE </strong>es responsable en caso de
            incumplimiento de sus obligaciones para con el <strong>CONTRATISTA</strong> en el desarrollo y ejecución
            del presente contrato.</p>
        <p style="text-align: justify;"><strong>NINGUNA DE LAS PARTES</strong> será responsable frente a la otra o
            frente a terceros por da&ntilde;os especiales, imprevisibles o da&ntilde;os indirectos, derivados de fuerza
            mayor, caso fortuito o circunstancias especiales atribuibles a terceros y ajenas a la labor principal de cada
            una de las partes en el desarrollo del objeto del presente contrato.</p>
        <p style="text-align: justify;"><strong>EL CONTRATISTA</strong> mantendrá indemne a <strong>EL
                CONTRATANTE</strong> de cualquier reclamación por circunstancias, fallas e imprevistos que puedan
            presentarse, que no sean de responsabilidad de <strong>EL CONTRATANTE</strong> y que sean atribuibles a terceros
            en el desarrollo del objeto del presente contrato.</p>
    
    
        <p style="text-align: justify;"><strong>DÉCIMA TERCERA: &nbsp;TERMINACIÓN</strong></p>
        <p style="text-align: justify;">El presente contrato podrá darse por terminado por mutuo acuerdo entre las
            partes, o en forma unilateral por el incumplimiento de las obligaciones derivadas del contrato, por cualquiera
            de ellas.</p>
        <p style="text-align: justify;">&nbsp;</p>
        <ol style="text-align: justify;">
            <li><strong>POR MUTUO ACUERDO.</strong> &nbsp;&nbsp;Por mutuo acuerdo las partes podrán dar por terminado
                este contrato en cualquier tiempo, expresada en el régimen de terminación que contiene el
                presente contrato.</li>
    
        <p>&nbsp;</p>
            <li><strong>DE PLENO DERECHO. &nbsp;</strong>&nbsp;Las partes podrán dar por terminado el contrato en
                cualquier momento y sin previo aviso, cuando una de las partes haya incurrido en el incumplimiento de
                cualquiera de las obligaciones pactadas en este documento, sin que medie caso fortuito o fuerza mayor.</li>
                <p>&nbsp;</p>
            <li><strong>POR JUSTA CAUSA</strong><strong>.</strong> &nbsp;Las partes podrán dar por terminado el
                presente contrato, sin generar indemnización por las siguientes razones:</li>
    
        <p> &nbsp;</p>
        <ol style="text-align: justify;">
    
                <li>Por violación del derecho de reserva sobre la información especialmente al uso personal o
                    de terceros de sus manuales operativos, instrucciones.</li>
                <li>Por el incumplimiento a cualquiera de los compromisos económicos que conllevan para las partes el
                    presente contrato.</li>
    
        </ol>
        <p>&nbsp;</p>
            <li><strong>POR EFECTOS LEGALES. &nbsp;</strong>En caso de vulneración a las disposiciones legales que
                actualmente o hacia futuro rigen el presente contrato, que hagan imposible para las partes continuar con la
                ejecución del mismo. &nbsp;En este caso no habrá lugar al pago de indemnización.</li>
        <p>&nbsp;</p>
            <li><strong>POR SENTENCIA JUDICIAL</strong> que así lo ordenare</li>
        </ol>
        <p style="text-align: justify;">La cesión o cualquier acto unilateral de disposición de alguna de las
            partes sobre el contrato, constituye causal de terminación unilateral del contrato con
            indemnización por la suma de diez (10) salarios mínimos legales vigentes mensuales en favor de la
            parte afectada.</p>
        <p style="text-align: justify;"><strong>DÉCIMA CUARTA:</strong> <strong>EXCLUSIÓN DE LA
                RELACIÓN LABORAL &nbsp;</strong></p>
        <p style="text-align: justify;">Queda claramente definido y establecido que <strong>EL</strong><strong>
                CONTRATISTA</strong> actuará por su propia cuenta, con absoluta autonomía y no estará
            sometido a subordinación laboral con <strong>EL CONTRATANTE</strong> y sus derechos se limitarán,
            de acuerdo con la naturaleza del contrato, únicamente a exigir el pago de los honorarios estipulados por
            la prestación del servicio mientras que <strong>EL CONTRATANTE </strong>se limitará a exigir el
            cumplimiento del objeto del contrato de conformidad con las especificaciones jurídicas y técnicas
            se&ntilde;aladas en el presente documento, sin ningún otro tipo de exigencia adicional.</p>
        <p style="text-align: justify;">Adicionalmente <strong>EL CONTRATISTA</strong> entiende y acepta que para el pago de
            sus servicios profesionales deberá acreditarle al <strong>CONTRATANTE</strong> el pago de su Seguridad
            Social en las fechas establecidas para tal efecto, para lo cual <strong>EL CONTRATISTA</strong> se compromete a
            efectuar estos aportes al Sistema General de Seguridad Social como profesional independiente, razón por
            la que <strong>NO</strong> existe ningún tipo de obligación sobre el particular a cargo del
            <strong>CONTRATANTE.</strong> Queda claramente entendido que <strong>NO</strong> existirá relación
            laboral alguna entre <strong>EL CONTRATANTE y EL CONTRATISTA,</strong> o el personal que éstos utilicen
            en la ejecución del objeto del presente contrato.</p>
    
        <p style="text-align: justify;"><strong>DÉCIMA QUINTA: CLÁUSULA PENAL</strong></p>
        <p style="text-align: justify;">En caso de incumplimiento total o parcial de las obligaciones del presente Contrato
            por cualquiera de las partes, la parte incumplida, deberá pagar a la parte cumplida a título de
            indemnización, una suma equivalente al <strong>DIEZ POR CIENTO (10%)</strong> del valor total del
            contrato.</p>
        <p style="text-align: justify;">El valor pactado de la presente cláusula penal es el de la estimación
            anticipada de perjuicios. En armonía con lo dispuesto por los 1602, y 1546 del Código Civil, en
            los contratos bilaterales, si uno de los contratantes no cumple lo pactado, opera la condición
            resolutoria y, en tal caso, por ministerio de la ley se faculta al otro contratante para pedir a su arbitrio, o
            el cumplimiento del contrato o su resolución, en ambos casos con la indemnización de perjuicios
            correspondiente.</p>
    
        <p style="text-align: justify;"><strong>DÉCIMA SEXTA: PROPIEDAD INTELECTUAL E INDUSTRIAL.</strong></p>
        <p style="text-align: justify;">Los derechos y el manejo de cada una de las partes sobre la propiedad intelectual e
            industrial se regularán de acuerdo con los siguientes parámetros:</p>
        <ul style="text-align: justify;">
            <li>De la propiedad intelectual que surja del desarrollo del presente contrato (desarrollos, software,
                códigos entre otros), el <strong>CONTRATISTA</strong> será titular de los derechos morales y
                <strong>EL CONTRATANTE</strong>de los derechos patrimoniales, al igual que el derecho a utilizar su
                contenido.</li>
            <li>Los derechos de propiedad sobre las marcas, nombres, logos y emblemas tanto del <strong>CONTRATANTE</strong>
                como del <strong>CONTRATISTA</strong> son de propiedad exclusiva de cada una de ellas, Por lo tanto, ninguna
                de las partes podrá utilizar la marca, el nombre, el logo y el emblema o cualquier otro signo que lo
                distinga sin que exista autorización previa de la otra parte, y su utilización no
                constituirá en ningún sentido derechos de propiedad intelectual sobre los mismos.</li>
        </ul>
    
        <p style="text-align: justify;"><strong>DÉCIMA SÉPTIMA. CONFIDENCIALIDAD Y MANEJO DE LA
                INFORMACIÓN. </strong></p>
        <p style="text-align: justify;">Las partes, sus empleados y/o personal se abstendrán de divulgar, publicar,
            comunicar directa o indirectamente a terceros o utilizar para propósitos diferentes a la correcta
            ejecución de este contrato, la información relacionada con la ejecución del mismo, por lo
            que convienen expresamente que toda información a la que tengan acceso o reciban en virtud del presente
            se considera confidencial y por lo tanto divulgar o transmitir la información considerada como tal,
            dará derecho a la parte cumplida a ejercer las acciones legales correspondientes. La obligación de
            confidencialidad a cargo de las partes y consagrada en la presente cláusula, subsistirá aun
            después de la terminación del presente contrato.</p>
        <p style="text-align: justify;"><strong>&nbsp;</strong></p>
        <p style="text-align: justify;"><strong>DÉCIMA OCTAVA: PRÓRROGA</strong></p>
        <p style="text-align: justify;">Si vencido el plazo establecido para la ejecución del contrato de
            prestación de servicios el <strong>CONTRATANTE</strong> decide ampliar el plazo de vencimiento, se
            elaborará un otrosí en tal sentido, el cual hará parte integral de este contrato.</p>
    
        <p style="text-align: justify;"><strong>DÉCIMA NOVENA: &nbsp;CESIONES</strong></p>
        <p style="text-align: justify;"><strong>EL CONTRATISTA</strong> no puede ceder parcial ni totalmente sus
            obligaciones o derechos derivados del presente contrato sin la autorización previa, expresa y escrita del
            <strong>CONTRATANTE, </strong>y en igual sentido con el <strong>CONTRATANTE. </strong></p>
    
        <p style="text-align: justify;"><strong>VIGÉSIMA: &nbsp;CASO FORTUITO O FUERZA MAYOR: </strong></p>
        <p style="text-align: justify;">Las partes quedan exoneradas de responsabilidad por el incumplimiento de cualquiera
            de sus obligaciones o por la demora en la satisfacción de cualquiera de las prestaciones a su cargo
            derivadas del presente contrato, cuando el incumplimiento sea resultado o consecuencia de la ocurrencia de un
            evento de fuerza mayor y caso fortuito debidamente invocadas y constatadas de acuerdo con la ley y la
            jurisprudencia colombiana.</p>
    
        <p style="text-align: justify;"><strong>VIGÉSIMA PRIMERA: NUEVO SERVICIO </strong></p>
        <p style="text-align: justify;">Si finalizado el objeto del servicio contratado, el <strong>CONTRATANTE</strong>
            necesita un nuevo servicio del <strong>CONTRATISTA,</strong> se deberá hacer un nuevo Contrato de
            Prestación de Servicios y no se entenderá como prórroga por desaparecer las causas
            contractuales que dieron origen al presente contrato.</p>
        <p style="text-align: justify;">&nbsp;<br /> <strong>VIGÉSIMA SEGUNDA: &nbsp;SOLUCIÓN DE
                CONTROVERSIAS: &nbsp;</strong></p>
        <p style="text-align: justify;">Las controversias o diferencias que surjan entre el<strong> CONTRATISTA </strong>y
            el<strong> CONTRATANTE</strong> con ocasión de la firma, ejecución, interpretación,
            prórroga o terminación del contrato, así como de cualquier otro asunto relacionado con el
            presente contrato, se intentará buscar en primera instancia un arreglo directo, el cual deberá
            efectuarse en un término no mayor a cinco (5) días hábiles a partir de la fecha en que
            cualquiera de las partes comunique por escrito a la otra la existencia de una diferencia.</p>
        <p style="text-align: justify;">Cuando la controversia no pueda solucionarse de manera directa por las partes
            involucradas luego de un periodo de <strong>TREINTA (30)</strong> días hábiles contados desde que
            una de las partes pone en conocimiento de la otra la respectiva controversia, el asunto debe someterse a un
            procedimiento conciliatorio que se surtirá ante un Centro de Conciliación debidamente autorizado
            por la ley para tal fin. Si en el término de ocho (8) días hábiles a partir del inicio del
            trámite de la conciliación, el cual se entenderá a partir de la fecha de la primera
            citación a las Partes, estas no llegan a un acuerdo para resolver sus diferencias, las mismas
            deberán acudir como última y definitiva instancia a la jurisdicción ordinaria.</p>
        <p style="text-align: justify;">En todo caso, este contrato presta mérito ejecutivo por ser una
            obligación clara, expresa y exigible para las partes.</p>
    
        <p style="text-align: justify;"><strong>VIGÉSIMA TERCERA: &nbsp;ANEXOS DEL CONTRATO</strong></p>
        <p style="text-align: justify;">Hacen parte integrante de este contrato los siguientes documentos:</p>
        <ul style="text-align: justify;">
            <li>Certificados de existencia y representación legal de cada una de las partes.</li>
            <li>Documentos, software, material técnico, manuales, desarrollos y demás relacionados y
                necesarios para la ejecución del objeto del contrato.</li>
            <li>Actas de inicio y de cierre del contrato</li>
            <li>Demás documentación relacionada con el objeto contractual.</li>
        </ul>
        <p style="text-align: justify;"><strong>VIGÉSIMA CUARTA: LEGISLACIÓN APLICABLE: </strong>El presente
            contrato se rige por las leyes de la República de Colombia, en especial por las normas de derecho privado
            aplicables, es decir, el Código Civil y Código de Comercio y las demás disposiciones
            aplicables al caso en concreto.</p>
        <p style="text-align: justify;"><br /> <strong>VIGÉSIMA QUINTA: DOMICILIO CONTRACTUAL: &nbsp;</strong></p>
        <p style="text-align: justify;">El domicilio contractual es para todos los efectos legales la ciudad de
            Bogotá D.C</p>`
        }
    }
}