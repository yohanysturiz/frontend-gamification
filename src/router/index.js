/* eslint-disable indent */
import Vue from 'vue'
import store from '../store'

import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router';
import SignUpTalent from '@/components/Auth/FormCandidate/SignUpTalent'
import SignUpOrganization from '@/components/Auth/FormClient/SignUpOrganization'
import RegisterClient from '@/components/Auth/FormClient/RegisterClient'
import ListCandidate from '@/components/Candidate/ListCandidate'
import ListPaymentAccount from '@/components/Candidate/ListPaymentAccount'
import Request from '@/components/Client/Request'
import ListRequest from '@/components/Client/ListRequest'
import ListContratos from '@/components/Client/ListContratos'
import ListContratosClient from '@/components/Client/ListContratosClient'
import ListLenguajes from '@/components/Client/ListLenguajes'
import Cv from '@/components/Candidate/Cv'
import FormClientRequest from '@/components/Client/FormClientRequest'
import FormLogin from '@/components/Auth/FormLogin.vue'
import ForgotPassword from '@/components/Auth/ForgotPassword'
import ChangePassword from '@/components/Auth/ChangePassword'
import Dashboard from '@/components/Admin/Dashboard'
import Clients from '@/components/Admin/Clients'
import Asignaciones from '@/components/Admin/Asignaciones'
import DashboardCandidate from '@/components/Candidate/Dashboard'
import PaymentAccount from '@/components/Candidate/PaymentAccount/PaymentAccount.vue'
import UpdatePaymentAccount from '@/components/Candidate/PaymentAccount/UpdatePaymentAccount.vue'
import LinkMeCompany from '@/components/Candidate/LinkMeCompany'
import TalentProfile from '@/components/Candidate/Profile/TalentProfile.vue'
import ProfileClient from '@/components/Client/Profile.vue'
import NotFound from '@/components/Layouts/NotFound.vue'
import CandidateSignature from '@/components/Candidate/CandidateSignature.vue'
import SelectedCandidateList from '@/components/Client/SelectedCandidateList.vue'
import ClientDashboard from '@/components/Client/Dashboard.vue'
import Adminchangepassword from '@/components/Admin/changepassword'
import PaymentAccountList from '@/components/PaymentAccountList'
import CandidateProfile from '@/components/Candidate/Profile/CandidateProfile'
import Layout from '@/components/Layouts/Breadcrumb/Layout'
import AvailableVacancies from '@/components/Candidate/AvailableVacancies/AvailableVacancies.vue'
import AllVacancies from '@/components/Candidate/AvailableVacancies/AllVacancies.vue'
import RequestCertificates from '@/components/Admin/ListCertificate.vue'
import AdminGamification from '@/components/Gamification/Admin.vue'
import AnalystGamification from '@/components/Gamification/AdminAnalyst.vue'
import DetailUserPoints from '@/components/Gamification/DetailUserPoints.vue'
import TargetsGamification from '@/components/Gamification/Targets.vue'
import TeamsGamification from '@/components/Gamification/Team/Teams.vue'
import TeamsAnalystDashGamification from '@/components/Gamification/Team/TeamsAnalystDash.vue'
import TeamDetail from '@/components/Gamification/Team/TeamDetail.vue'
import UsersManagment from '@/components/Client/Users/UsersManagment.vue'
import TargetDetail from '@/components/Gamification/Target/TargetDetail.vue'
import ConfigGamification from '@/components/Gamification/Config.vue'
import MilestonesGamification from '@/components/Gamification/Milestone/Milestones.vue'
import RewardsGamification from '@/components/Gamification/Rewards/Rewards.vue'
import LeaderBoard from '@/components/Gamification/LeaderBoard/LeaderBoard.vue'
import MyTargets from '@/components/Gamification/MyTargets/MyTargets.vue'
import MyJob from '@/components/Gamification/MyJob/MyJob.vue'

// import store from '../store';


// Vue.use(Router)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: FormLogin,
        meta: {
            requiresAuth: false
        }
    },
    {
        path: '/sign-in',
        name: 'FormLogin',
        component: FormLogin,
        meta: {
            requiresAuth: false
        }
    },
    {
        path: '/sign-in',
        name: 'FormLogin',
        component: FormLogin,
        meta: {
            requiresAuth: false
        }
    },
    {
        path: '/sign-up/talent',
        name: 'signUpTalent',
        component: SignUpTalent,
        meta: {
            requiresAuth: false
        }
    },
    {
        path: '/sign-up/organization',
        name: 'signUpOrganization',
        component: SignUpOrganization,
        meta: {
            requiresAuth: false
        }
    },
    {
        path: '/solicitar-desarrollador',
        name: 'FormClientRequest',
        component: FormClientRequest,
        meta: {
            requiresAuth: false
        }
    },
    {
        path: '/registro-empresa',
        name: 'registroEmpresa',
        component: RegisterClient,
        meta: {
            requiresAuth: false
        }
    },
    {
        path: '/recovery-password',
        name: 'ForgotPassword',
        component: ForgotPassword,
        meta: {
            value: "Recuperar Contraseña",
            requiresAuth: false
        }
    },


    //*****ADMIN DASHBOARD ROUTES******//
    {
        path: '/dashboard',
        name: '',
        component: Layout,
        meta: {
            value: "Dashboard",
            requiresAuth: true,
            rol: ['administrator', 'analyst']
        },
        children: [{
                path: '/dashboard',
                name: 'Dashboard',
                component: Dashboard,
                meta: {
                    requiresAuth: true,
                    rol: ['administrator', 'analyst', 'talent', 'collaborator']
                }
            },
            {
                path: '/change-password-admin',
                name: 'Adminchangepassword',
                component: Adminchangepassword,
                meta: {
                    value: "Cambiar contraseña",
                    requiresAuth: true,
                    rol: ['administrator']
                }
            },
            {
                path: '/solicitudes',
                name: 'ListRequest',
                component: ListRequest,
                meta: {
                    value: "Vacantes Solicitadas",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst']
                }
            },
            {
                path: '/lista-certificados',
                name: 'RequestCertificates',
                component: RequestCertificates,
                meta: {
                    value: "Certificados Solicitados",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst']
                }
            },
            {
                path: '/lista-clientes',
                name: '',
                component: Layout,
                meta: {
                    value: "Lista de Clientes",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst']
                },
                children: [{
                        path: '/lista-clientes',
                        name: 'Clients',
                        component: Clients,
                        meta: {
                            requiresAuth: true,
                            rol: ['administrator', 'analyst']
                        },
                    },
                    {
                        path: '/asignaciones-logs',
                        name: 'Asignaciones',
                        component: Asignaciones,
                        meta: {
                            requiresAuth: true,
                            rol: ['administrator', 'analyst']
                        },
                    },
                    {
                        path: '/imukofrontend/client/:id',
                        name: 'ProfileClient',
                        component: ProfileClient,
                        meta: {
                            value: "Administrar cliente",
                            params: true,
                            requiresAuth: true,
                            rol: ['administrator', 'analyst']
                        }
                    },
                ]
            },
            {
                path: '/lista-candidatos',
                name: '',
                component: Layout,
                meta: {
                    value: "Lista Candidatos",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst']
                },
                children: [{
                        path: '/lista-candidatos',
                        name: 'ListCandidate',
                        component: ListCandidate,
                        meta: {
                            requiresAuth: true,
                            rol: ['administrator', 'analyst']
                        }
                    },
                    {
                        path: '/candidate-profile/:id',
                        name: 'CandidateProfile',
                        component: CandidateProfile,
                        props: true,
                        meta: {
                            value: "Perfil candidato",
                            params: true,
                            requiresAuth: true,
                            rol: ['administrator', 'analyst']
                        }
                    },
                ]
            },
            {
                path: '/Lenguajes',
                name: 'ListLenguajes',
                component: ListLenguajes,
                meta: {
                    value: "Lenguajes y Frameworks",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst']
                }
            },
            {
                path: '/Contratos',
                name: 'ListContratos',
                component: ListContratos,
                meta: {
                    value: "Lista de Contratos",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst']
                }
            },
            // Analyst
            {
                path: '/analyst/gamification',
                name: 'Analyst gamification',
                component: AnalystGamification,
                meta: {
                    value: "Gamification Analyst",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst']
                }
            },
            {
                path: '/analyst/gamification/team',
                name: 'Analyst Teams',
                component: TeamsAnalystDashGamification,
                meta: {
                    value: "Teams",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst','client']
                }
            },

            // Admin
            {
                path: '/admin/gamification',
                name: 'Gamification',
                component: AdminGamification,
                meta: {
                    value: "Gamification Admin",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst', 'client']
                }
            },
            
            {
                path: '/admin/gamification/target',
                name: 'Target',
                component: TargetsGamification,
                meta: {
                    value: "Targets",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst', 'client']
                }
            },
            {
                path: '/admin/gamification/team',
                name: 'Team',
                component: TeamsGamification,
                meta: {
                    value: "Teams",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst','client']
                }
            },
            {
                path: '/admin/gamification/team/:id/detail',
                name: 'TeamDetail',
                component: TeamDetail,
                meta: {
                    value: "Teams",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst', 'client']
                }
            },
            {
                path: '/admin/gamification/target/:id/detail',
                name: 'TargetDetail',
                component: TargetDetail,
                meta: {
                    value: "Targets",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst', 'client']
                }
            },
            {
                path: '/admin/gamification/milestones',
                name: 'Milestones',
                component: MilestonesGamification,
                meta: {
                    value: "Milestones",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst','client']
                }
            },
            {
                path: '/admin/gamification/rewards',
                name: 'Rewards',
                component: RewardsGamification,
                meta: {
                    value: "Reward",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst','client']
                }
            },
            {
                path: '/admin/users',
                name: 'AdminUsers',
                component: UsersManagment,
                meta: {
                    value: "Users",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst', 'client']
                }
            },
            {
                path: '/admin/config',
                name: 'AdminConfig',
                component: ConfigGamification,
                meta: {
                    value: "Users",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst', 'client']
                }
            },
            {
                path: '/Contratos_Client',
                name: 'ListContratosClient',
                component: ListContratosClient,
                meta: {
                    value: "Contratos de Clientes",
                    requiresAuth: true,
                    rol: ['administrator']
                }
            },
            {
                path: '/admin-cuentas-de-cobro',
                name: 'PaymentAccountList',
                component: PaymentAccountList,
                meta: {
                    value: "Cuentas de Cobro",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst']
                }
            },
        ]
    },
    //*****CANDIDATE DASHBOARD ROUTES******//
    {
        path: '/talent',
        name: '',
        component: Layout,
        meta: {
            value: "Me profile",
        },
        children: [
            {
                path: '/talent/profile',
                name: 'TalentProfile',
                component: TalentProfile,
                meta: {
                    requiresAuth: true,
                    rol: ['candidate', 'talent', 'administrator', 'collaborator']
                }
            },
            {
                path: '/cuentas-de-cobro',
                name: '',
                component: Layout,
                meta: {
                    value: "Mis cuentas de cobro",
                    requiresAuth: true,
                    rol: ['administrator', 'candidate', 'analyst']
                },
                children: [{
                        path: '/cuentas-de-cobro',
                        name: 'ListPaymentAccount',
                        component: ListPaymentAccount,
                        meta: {
                            value: "",
                            requiresAuth: true,
                            rol: ['administrator', 'candidate', 'analyst']
                        },
                    },
                    {
                        path: '/payment-account',
                        name: 'PaymentAccount',
                        component: PaymentAccount,
                        meta: {
                            value: "Generar cuenta de cobro",
                            requiresAuth: true,
                            rol: ['candidate', 'administrator', 'analyst']
                        }
                    },
                ]
            },
            {
                path: '/me-contracts',
                name: 'ListContratos',
                component: ListContratos,
                meta: {
                    value: "Lista de Contratos",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst', 'candidate']
                }
            },
            {
                path: '/gamification/user',
                name: 'Gamification Detail ',
                component: DetailUserPoints,
                meta: {
                    value: "Detail User Points",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst']
                }
            },
            {
                path: '/talent/profile/leaderboard',
                name: 'LeaderBoard Detail ',
                component: LeaderBoard,
                meta: {
                    value: "Detail User Points",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst', 'candidate', 'collaborator']
                }
            },
            {
                path: '/talent/profile/mytargets',
                name: 'MyTargets',
                component: MyTargets,
                meta: {
                    value: "Detail User Points",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst', 'candidate', 'collaborator']
                }
            },
            {
                path: '/talent/profile/myjob',
                name: 'MyJob',
                component: MyJob,
                meta: {
                    value: "Detail User Points",
                    requiresAuth: true,
                    rol: ['administrator', 'analyst', 'candidate', 'collaborator']
                }
            },
            {
                path: '/available-vacancies',
                name: '',
                component: Layout,
                meta: {
                    value: "Vacantes",
                    requiresAuth: true,
                    rol: ['candidate']
                },
                children: [{
                        path: '/available-vacancies',
                        name: '',
                        component: AvailableVacancies,
                        meta: {
                            value: "",
                            requiresAuth: true,
                            rol: ['candidate']
                        },
                    },
                    {
                        path: '/vacancies/:vacanciesType',
                        name: 'AllVacancies',
                        component: AllVacancies,
                        meta: {
                            value: "Tus vacantes",
                            params: true,
                            requiresAuth: true,
                            rol: ['candidate']
                        }
                    },
                ]
            },
        ],
    },

    //*****CLIENT DASHBOARD ROUTES*****//

    {
        path: '/client-dashboard',
        name: '',
        component: Layout,
        meta: {
            value: "Mi perfil",
        },
        children: [{
                path: '/client-dashboard',
                name: 'ClientDashboard',
                component: ClientDashboard,
                meta: {
                    requiresAuth: true,
                    rol: ['client']
                },
            },
            {
                path: '/solicitud-desarrollador',
                name: 'FormClientRequest',
                component: FormClientRequest,
                meta: {
                    value: "Solicitar Desarrollador",
                    requiresAuth: false
                }
            },

        ]
    },

    //***********************************//
    {
        path: '/selected-candidates/:id',
        name: 'SelectedCandidateList',
        component: SelectedCandidateList,
        meta: {
            requiresAuth: false
        }
    },
    {
        path: '/reset-password',
        name: 'ChangePassword',
        component: ChangePassword,
        meta: {
            requiresAuth: false
        }
    },

    {
        path: '/imukofrontend/candidates/:candidateId/cv',
        name: 'Cv',
        component: Cv,
        meta: {
            requiresAuth: true,
            rol: ['administrator', 'analyst']
        }
    },
    {
        path: '/dashboard-talent',
        name: 'DashboardTalent',
        component: DashboardCandidate,
        meta: {
            requiresAuth: true,
            rol: ['talent', 'analyst']
        }
    },
    {
        path: '/vinculacion-empresa',
        name: 'LinkMeCompany',
        component: LinkMeCompany,
        meta: {
            requiresAuth: true,
            rol: ['candidate', 'administrator', 'analyst']
        }
    },
    {
        path: '/update-payment-account',
        name: 'UpdatePaymentAccount',
        component: UpdatePaymentAccount,
        meta: {
            requiresAuth: true,
            rol: ['candidate', 'administrator', 'analyst']
        }
    },
    {
        path: '/solicitudes/:id',
        name: 'Request',
        component: Request,
        props: true,
        meta: {
            requiresAuth: true,
            rol: ['administrator', 'analyst']
        }
    },
    {
        path: '/candidate-signature/:id',
        name: 'CandidateSignature',
        component: CandidateSignature,
        meta: {
            requiresAuth: false
        }
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

router.beforeEach((to, from, next) => {
    if (to.meta.requiresAuth && store.getters.isLoggedIn) {
        console.log("Logged")
        // let user = JSON.parse(store.state.user);
        // if (to.matched.some(record => record.meta.rol && record.meta.rol.includes(user.roles))) {
        //     next()
        // } else {
        //     next(FormLogin)
        // }
        next()
    } else {
        // router.push({ name: 'FormLogin' })
        console.log("Don't Logged")
        next();
    }
})

export default router
