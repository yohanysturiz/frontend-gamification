// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import { createApp } from 'vue';
import router from './router'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import HighchartsVue from 'highcharts-vue'
import App from './App.vue';

/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'

/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

/* import specific icons */
import { 
    faUserSecret, 
    faCode,
    faFileInvoice,
    faArrowRightToBracket,
    faTrophy,
    faSignature,
    faFileSignature,
    faBriefcase,
    faDoorOpen,
    faHome,
    faUsers,
    faTrash,
    faGear,
    faCirclePlus,
    faCircleXmark,
    faPeopleGroup,
    faFileCsv,
    faArrowLeft,
    faCube,
    faCoins,
    faBullseye,
    faGift,
    faArrowRight,
    faPaperPlane
} from '@fortawesome/free-solid-svg-icons'

import { 
    faGoogle,
    faGithub,
    faLinkedinIn,
    faTwitter
} from '@fortawesome/free-brands-svg-icons'

import store from './store'
// import GAuth from 'vue-google-oauth2';
// import VueAuthenticate from 'vue-authenticate'
// import payments from './components/PaymentAccount/payments-methods';


// const gauthOption = {
//     clientId: '927532584923-1lb0o4haq6lekte9q29lbuk04ufmq3tv.apps.googleusercontent.com',
//     scope: 'profile email',
//     prompt: 'select_account'
// }

// const VueAuthenticateOptions = {
//     baseUrl: 'https://app.imuko.co', // Your API domain
//     tokenPath: 'data.user.api_token',
//     providers: {
//         github: {
//             clientId: '544257978eddb4e7cad0',
//             url: 'https://backend.imuko.co/api/auth/github',
//             redirectUri:'https://app.imuko.co/callback',


//         },
//         linkedin: {
//             clientId: '78fku118q6a11q',
//             url: 'https://backend.imuko.co/api/auth/linkedin',
//             requiredUrlParams: ['state', 'scope'],
//             redirectUri:'https://app.imuko.co',
//             scope:['r_emailaddress', 'r_liteprofile'],

//         },
//         google:{
//             clientId: '927532584923-1lb0o4haq6lekte9q29lbuk04ufmq3tv.apps.googleusercontent.com',
//             url: 'https://backend.imuko.co/api/auth/google',
//             requiredUrlParams: ['scope'],
//             redirectUri:'https://app.imuko.co/callback',
//             scope: ['profile', 'email'],

//         }

//     }
// }

/* add icons to the library */
library.add(
    faUserSecret,
    faCode,
    faArrowRightToBracket,
    faGoogle,
    faGithub,
    faLinkedinIn,
    faTrophy,
    faFileSignature,
    faFileInvoice,
    faBriefcase,
    faTwitter,
    faDoorOpen,
    faHome,
    faUsers,
    faTrash,
    faGear,
    faCirclePlus,
    faCircleXmark,
    faPeopleGroup,
    faFileCsv,
    faArrowLeft,
    faArrowRight,
    faPaperPlane,
    faCube,
    faCoins,
    faBullseye,
    faGift,
)

const app = createApp(App);
app.use(router);
app.use(ElementPlus);
app.use(HighchartsVue)
app.use(store)
app.component('font-awesome-icon', FontAwesomeIcon)
// app.use(VueAuthenticate, VueAuthenticateOptions);
// app.use(GAuth, gauthOption);
// app.use(payments);

app.config.productionTip = false

app.mount('#app');
