const path = require('path');

module.exports = {
  publicPath: '/',
  
  // Configuración para el servidor de desarrollo
  devServer: {
    // Permite el acceso a la aplicación desde otros dispositivos en la red
    host: '0.0.0.0',
    port: 8080,
  },

  // Configuración de Vue CLI
  configureWebpack: {
    // Agrega la extensión '.vue' a los imports de componentes
    resolve: {
      alias: {
        vue$: 'vue/dist/vue.esm-bundler.js',
      },
    },
    stats: {
        children: true,
    },
  },

  // Configuración de Vue Router
  chainWebpack: (config) => {
    // Agrega una regla para las imágenes y las incluye en el bundle
    config.module
    .rule('images')
    .use('url-loader')
    .tap((options) => {
      // Verifica si options y fallback están definidos antes de acceder a sus propiedades
      if (options && options.fallback && options.fallback.options) {
        options.fallback.options.name = 'img/[name].[hash:8].[ext]';
      }
      return options;
    });

    // Configura el HTML plugin para utilizar el título del sitio y el template base
    config.plugin('html').tap((args) => {
        args[0].title = 'Mi sitio web';
        args[0].template = path.resolve(__dirname, 'public', 'index.html');
        return args;
    });
  },

  // Configuración de Element Plus
  pluginOptions: {
    // Configura el loader de Element Plus para utilizar el tema predeterminado
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [
        path.resolve(__dirname, 'src', 'styles', 'element-variables.scss'),
      ],
    },
  },
};